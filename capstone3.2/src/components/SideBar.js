import React from 'react';
import { SideBarData } from './SideBarData';
import { Link } from 'react-router-dom';
import { Navbar} from 'react-bootstrap';

function SideBar() {
  return (
    <Navbar expand="lg" className="Sidebar">
      <Navbar.Toggle aria-controls="sidebar-nav" />
      <Navbar.Collapse id="sidebar-nav">
        <div className="sideBarList">
          {SideBarData.map((item, key) => {
            return (
              <div className="sidePanel" key={key}>
              {item.icon}
              <Link to={item.link}>             
                  <button className="sideBarButton"> {item.title}</button>
                </Link>
              </div>
            );
          })}
        </div>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default SideBar;
