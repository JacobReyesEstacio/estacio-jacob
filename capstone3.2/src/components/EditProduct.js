import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function EditProduct({product, fetchData}){


	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);


	const openEdit = (productId) =>{

		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{

			setProductId(data._id);
			setName(data.productName);
			setDescription(data.productDescription);
			setPrice(data.productPrice);
		})

			setShowEdit(true);

	}

	const closeEdit = () =>{

		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);

	}

	const editProduct = (e, productId)=>{

		e.preventDefault();

		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/updateProduct/${productId}`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productName:name,
				productDescription: description,
				productPrice:price
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Product Successfully Updated'
				})
				closeEdit();
				fetchData()

			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				closeEdit();
				fetchData()

			}
		})


	}


	return(

		<>

			<Button variant="warning" className='shadowed' size="sm" onClick={()=>openEdit(product)}>Edit</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form className='shadowed' onSubmit={e=>editProduct(e,productId)}>

					<Modal.Header closeButton>
						<Modal.Title className='custom-heading'>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<Form.Group controlId="productName">
							<Form.Label className='custom-heading my-1'>Name</Form.Label>
							<Form.Control
								type="text"
								value={name}
								onChange={e=>setName(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label className='custom-heading my-3'>Description</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={e=>setDescription(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label className='custom-heading my-3'>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={e=>setPrice(e.target.value)}
								required/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" className='shadowed' onClick={closeEdit}>Close</Button>
						<Button variant="success" className='shadowed' type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>

		</>

	)


}