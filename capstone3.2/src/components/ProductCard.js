import React from "react";
import { Card, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import chocodonut from "../images/chocodonut.jpg";

export default function ProductCard(props) {
  let str = "\u20B1";
  const product = props.product;
  

  return (
    <Container fluid>
      <Card className="shadowed" as={Link} to={`/products/${product._id}`}>
        <img src={chocodonut} alt="donut" className="shadowed" />
        <Card.Body>
          <Card.Title>
            {product.productName}
          </Card.Title>
          <Card.Text>{product.productDescription}</Card.Text>
          <Card.Text>
            {str}
            {product.productPrice}
          </Card.Text>
        </Card.Body>
      </Card>
    </Container>
  );
}
