import { useState, useEffect } from 'react';
import { Table, Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarDays, faCartShopping, faCoins } from '@fortawesome/free-solid-svg-icons';


let str = "\u20B1";
const UserOrders = ({ userOrderData }) => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        const ordersArr = userOrderData.map((order) => (
            <tr key={order._id}>
                <td>
                    <Row>
                        <Col>
                            <p>Products:</p>
                        </Col>
                        <Col>
                            <p>Quantity:</p>
                        </Col>
                    </Row>
                    {order.orderProducts.map((product) => (
                        <Row key={product.productId}>
                            <Col>
                                <p>{product.productId}</p>
                            </Col>
                            <Col>
                                <p>{product.quantity}</p>
                            </Col>
                        </Row>
                    ))}
                </td>
                <td>{str}{order.totalAmount}</td>
                <td>{order.purchasedOn}</td>
            </tr>
        ));
        setOrders(ordersArr);
    }, [userOrderData]);

    return (
        <Container fluid>
            <h1 className='my-5 text-center custom-heading'> Order History</h1>
            <div style={{ overflowX: 'auto', overflowY: 'auto', maxHeight: '500px' }}>
            <Table className='table-secondary' striped bordered hover responsive>
                <thead>
                    <tr>
                        <th><FontAwesomeIcon className='mx-2' icon={faCartShopping} />Orders</th>
                        <th><FontAwesomeIcon className='mx-2' icon={faCoins} />Amount</th>
                        <th><FontAwesomeIcon className='mx-2' icon={faCalendarDays} />Date</th>
                    </tr>
                </thead>
                <tbody>{orders}</tbody>
            </Table>
            </div>
        </Container>
    );
}

export default UserOrders;
