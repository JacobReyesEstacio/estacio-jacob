import React from 'react'
import { Row, Col } from 'react-bootstrap'
import ProductCard from '../ProductCard'


const UserView = ({productsData, fetchActiveData}) => {

   

  return (
<>
    <h1 align="center" className='p-3 custom-heading'>Our Products</h1>
    <Row xs={1} md={3} className="g-4">
        {productsData.map((product, idx)=> (
            <Col align="center" key={idx}>
                <ProductCard product={product} fetchActiveData={fetchActiveData} />
            </Col>
        ))}
    </Row>
</>
  )
}

export default UserView
