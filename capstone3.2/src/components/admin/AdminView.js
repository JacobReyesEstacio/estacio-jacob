import { useState, useEffect } from 'react';
import { Table, Container, Row, Col } from 'react-bootstrap';
import ArchiveProducts from '../ActivateProduct';
import EditProduct from '../EditProduct';
import Layout from '../Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserTie, faCoins, faUserTag, faScaleBalanced, faOutdent, faBan, faUserGear } from '@fortawesome/free-solid-svg-icons';



const AdminView = ({productsData, fetchData}) => {

    const [products, setProducts] = useState([]);

    useEffect (() => {
        const productsArr = productsData.map((product) => {
            return (
                <tr key={product._id}>
                    <td className="d-none d-sm-table-cell">{product._id}</td>
                    <td>{product.productName}</td>
                    <td className="d-none d-md-table-cell">{product.productDescription}</td>
                    <td>{product.productPrice}</td>
                    <td className={product.isActive ? "text-success" : "text-secondary"}>
                        {product.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td><EditProduct product={product._id} fetchData={fetchData}/></td>  
                    <td><ArchiveProducts productId={product._id} isActive={product.isActive} fetchData={fetchData}/></td> 
                </tr>
            )
        })
        setProducts(productsArr);
    }, [productsData]) 
   

  return (
    <Layout>
            <Container fluid>   
                             
                                <h1 className='my-5 me-5 text-center custom-heading'>
                                <FontAwesomeIcon className='mx-2' icon={faUserGear} /> Admin Dashboard</h1>
                                <div className="table-responsive" style={{ overflowX: 'auto', overflowY: 'auto', maxHeight: '500px', maxWidth: '1100px' }}>     
                                <Table striped bordered hover responsive="sm" className='table table-secondary shadowed' >
                                            <thead>
                                                <tr className="text-center">
                                                    <th className="d-none d-sm-table-cell table-dark">
                                                    <FontAwesomeIcon className='mx-2' icon={faUserTag} />ID</th>
                                                    <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faUserTie} />Name</th>
                                                    <th className="d-none d-md-table-cell table-dark">
                                                    <FontAwesomeIcon className='mx-2' icon={faOutdent} />Description</th>
                                                    <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCoins} />Price</th>
                                                    <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faBan} />Availability</th>
                                                    <th className='table-dark' colSpan="2"><FontAwesomeIcon className='mx-2' icon={faScaleBalanced} />Actions</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {products}
                                            </tbody>
                                        </Table>    
                            </div>
                </Container>
    </Layout>
          
  )
}

export default AdminView
