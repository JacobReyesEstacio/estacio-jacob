import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import Layout from '../Layout';
import PromoteAdmin from './PromoteAdmin';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserTie, faUserTag, faVault, faTurnUp, faUserGear } from '@fortawesome/free-solid-svg-icons';


const AllUsers = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem("token");

    fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/users/allUsers`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (Array.isArray(data)) {
          setUsers(data);
        } else {
          console.error("API response is not an array:", data);
        }
      })
      .catch((error) => {
        console.error("Error fetching User Data:", error);
      });
  }, [users]);

  return (
    <Layout>
      <Container fluid>
      
        <h1 className='my-5 text-center custom-heading'>
        <FontAwesomeIcon className='mx-2' icon={faUserGear} /> Admin Dashboard</h1>
        <div className="table-responsive" style={{ overflowX: 'auto', overflowY: 'auto', maxHeight: '500px', maxWidth: '1100px' }}>
        <Table  striped bordered hover responsive="sm" className='table-secondary shadowed'>
          <thead>
            <tr className="text-center">
              <th className="d-none d-md-table-cell table-dark"><FontAwesomeIcon className='mx-2' icon={faUserTag} /> ID</th>
              <th className="table-dark"><FontAwesomeIcon className='mx-2' icon={faUserTie} />Name</th>
              <th className="d-none d-sm-table-cell table-dark"><FontAwesomeIcon className='mx-2' icon={faVault} />ADMIN</th>
              <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faTurnUp} />Promote</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr key={user._id}>
                <td className="d-none d-md-table-cell">{user._id}</td>
                <td>{user.email}</td>
                <td className={user.isAdmin ? "text-success d-none d-sm-table-cell" : "text-secondary d-none d-sm-table-cell"}>
                  {user.isAdmin ? "YES" : "NO"}
                </td>
                <td><PromoteAdmin userId={user._id} isAdmin={user.isAdmin} /></td>
              </tr>
            ))}
          </tbody>
        </Table>
        </div>
      </Container>
    </Layout>
  );
}

export default AllUsers;
