import { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';
import Layout from '../Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserTag, faCoins, faCalendarDays, faSackDollar, faUserGear } from '@fortawesome/free-solid-svg-icons';


let str = "\u20B1";

const AdminOrders = ({ ordersData }) => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        const ordersArr = ordersData.map((order) => {
            return (
                <tr key={order._id}>
                <td className="d-none d-sm-table-cell">{order._id}</td> 
                    <td>{order.orderUserId}</td>
                    <td>{str}{order.totalAmount}</td>
                    <td>{order.purchasedOn}</td>
                </tr>
            )
        })
        setOrders(ordersArr);
    }, [ordersData])

    return (
        <Layout>
            <Container fluid>
                <h1 className='my-5 text-center custom-heading'>
                <FontAwesomeIcon className='mx-2' icon={faUserGear} /> Admin Dashboard</h1>
                <div className="table-responsive" style={{ overflowX: 'auto', overflowY: 'auto', maxHeight: '500px', maxWidth: '1100px' }}>             
                        <Table striped bordered hover responsive="sm" className="table table-secondary shadowed">
                    <thead>
                        <tr>
                            <th className="d-none d-sm-table-cell table-dark"><FontAwesomeIcon className='mx-2' icon={faSackDollar} />Sale ID</th>
                            <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faUserTag} />User ID</th>
                            <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCoins} />Amount</th>
                            <th className='table-dark'><FontAwesomeIcon className='mx-2' icon={faCalendarDays} />Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders}
                    </tbody>
                </Table>
                        </div>




           
            </Container>
        </Layout>
    )
}

export default AdminOrders;
