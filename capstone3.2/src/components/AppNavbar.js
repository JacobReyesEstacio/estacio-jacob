import { useContext, useState, useEffect } from "react";
import { Navbar, Nav, Container, Button, Modal, Row, Col } from "react-bootstrap";
import { Link, NavLink } from 'react-router-dom';
import UserContext from "../UserContext";
import { useCartData } from "../CartContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping, faHome, faWheatAwn } from '@fortawesome/free-solid-svg-icons';
import UpdateCart from "./UpdateCart";
import { useProductData } from "../ProductContext";



export default function AppNavbar () {


    const { user } = useContext(UserContext);
    const [show, setShow] = useState(false);
    const [checkedItems, setCheckedItems] = useState([]);
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);
    const cartData = useCartData();
    const productData = useProductData();

    const handleCheckboxToggle = (productId) => {
        setCheckedItems((prevCheckedItems) => ({
          ...prevCheckedItems,
          [productId]: !prevCheckedItems[productId],
        }));
      };

      const [itemPrice, setItemPrice] = useState([]);
      const [totalPrice, setTotalPrice] = useState([]); 
      const [grandTotal, setGrandTotal] = useState();
       useEffect(() => {
        calculatePrices();
    }, [cartData]); 

     const calculatePrices = () => {
      const newPrice = {};
      const newTotalPrice = {};
    
      cartData.forEach((cartItem) => {
          // Find the product if it exists in productData
          const item = productData.find((product) => product._id === cartItem.productId);
          
          if (item) {
              newPrice[cartItem.productId] = item.productPrice;
              newTotalPrice[cartItem.productId] = item.productPrice * cartItem.quantity;
          } else {
              // Handle the case where the product is not found
              newPrice[cartItem.productId] = 0; // or any default value
              newTotalPrice[cartItem.productId] = 0; // or any default value
          }
      });
  
      setItemPrice(newPrice);
      setTotalPrice(newTotalPrice);
      const totalSum = Object.values(totalPrice).reduce((acc, price) => acc + price, 0);
      setGrandTotal(totalSum);
  };
 
   

  

    return(
        <>
        <Navbar className="navMain" expand="lg" data-bs-theme="dark">
        <Container>
            <Navbar.Brand className="custom-heading" as={Link} to="/">
            <FontAwesomeIcon className='mx-2' icon={faWheatAwn} />
                  Pâtisserie Flourique
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
					   <Nav.Link as={Link} to="/"> <FontAwesomeIcon className='mx-2' icon={faHome} />Home</Nav.Link>
					   <Nav.Link as={Link} to="/products">
                       {
                        (user.isAdmin) ? "Dashboard"
                        : "Products"
                       }
                       
                        </Nav.Link>
					            	{
                            (user.id !== null) ?

                            user.isAdmin
                            ?
                            <>
						            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                            </>
                            :
                            <>
                                    <Nav.Link as={Link} to="orders">Orders</Nav.Link>
						                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                    <Button variant="outline-info" className="ms-3" onClick={handleShow}>
                                    <FontAwesomeIcon className='mx-2' icon={faCartShopping} />
                                    Cart {cartData.length}</Button>
                            </>
                            :
                            <>
                                   <Nav.Link as={Link} to="/login">Login</Nav.Link>
						                       <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            </>
                        }
						            
						        
						               
						        
						              
					   </Nav>
            </Navbar.Collapse>
            </Container>
          </Navbar>


          <Modal className="shadowed" show={show} onHide={handleClose}>
          <Modal.Header>
            <Modal.Title>Your Cart</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {cartData.map((cartItem, idx) => (
              <Row key={idx}>
                <Col align="center">
                  <input
                    type="checkbox"
                    checked={checkedItems[cartItem.productId] || false}
                    onChange={() => handleCheckboxToggle(cartItem.productId)}
                  />
                  <h6>{cartItem.productId}</h6>
                  <p>{cartItem.quantity}</p>
                  <UpdateCart productId={cartItem.productId} cartArray={cartData} />
                  <p>Price each: {itemPrice[cartItem.productId]}</p>
                   <p>Total:{totalPrice[cartItem.productId]} </p>
                </Col>
              </Row>
            ))}
            <h3 className="grandTotal">Total: {grandTotal}</h3>
          </Modal.Body>
        </Modal>
    </>
    )
}