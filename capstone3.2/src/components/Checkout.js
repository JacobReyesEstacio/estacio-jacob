import React from 'react'
import { useParams } from 'react-router-dom';


function Checkout() {


    const checkOut = () => {
		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/orders/checkout`, {
		  method: 'POST',
		  headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem('token')}`
		  },
		  body: JSON.stringify({
			orderProducts: [
			  {
				productId: productId,
				quantity: 1
			  }
			]
		  })
		})
		.then(res => res.text())
		.then(responseText => {
		  if (responseText === "Order placed successfully!") {
			Swal.fire({
			  title: "Successfully ordered",
			  icon: 'success',
			  text: "You have successfully ordered the product."
			});
		  } else {
			Swal.fire({
			  title: "Order failed",
			  icon: 'info',
			  text: "Please Login to Order."
			});
	
		  }
		})
		.catch(error => {
		  console.error(error);
		  Swal.fire({
			title: "Something went wrong",
			icon: 'error',
			text: "Please try again."
		  });
		});
	  }






  return (
    <div>
      
    </div>
  )
}

export default Checkout
