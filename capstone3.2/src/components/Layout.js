// Layout.js
import React from 'react';
import SideBar from './SideBar';

const Layout = ({ children }) => {
  return (
    <div className="d-flex">
      <SideBar />
      <div className="flex-grow-1">
        {children}
      </div>
    </div>
  );
}

export default Layout;
