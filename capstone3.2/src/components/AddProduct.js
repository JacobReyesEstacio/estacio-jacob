import { useState, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Layout from './Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCirclePlus } from '@fortawesome/free-solid-svg-icons';

export default function AddProduct() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  // Input states
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  function createProduct(e) {
    // Prevent submit event's default behavior
    e.preventDefault();

    let token = localStorage.getItem('token');
    console.log(token);

    fetch('https://cpston2-ecommerceapi-estacio.onrender.com/products/addProduct', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        productName: name,
        productDescription: description,
        productPrice: price
      })
    })
      .then(res => res.json())
      .then(data => {
        // Data is the response of the API/server after it's been processed as a JS object through our res.json() method.
        console.log(data);

        if (data) {
          Swal.fire({
            icon: "success",
            title: "Product Added"
          });

          navigate("/products");
        } else {
          Swal.fire({
            icon: "error",
            title: "Unsuccessful Product Creation",
            text: data.message
          });
        }
      });

    setName("");
    setDescription("");
    setPrice(0);
  }

  return (

    <Layout>
  
                  <Container fluid>
                  <Row className="justify-content-md-center">
                    <Col sm={10} md={9}>
                    <h1 className='pt-5 pb-3 text-center custom-heading'>
                    <FontAwesomeIcon className='mx-2' icon={faCirclePlus} />Add Product</h1>
                      <Form size-lg="sm" size-sm="lg" className="shadowed bg-light text-dark p-3 rounded custom-heading" onSubmit={e => createProduct(e)}>
                        {[
                          { label: "Name", type: "text", value: name, onChange: setName },
                          { label: "Description", type: "text", value: description, onChange: setDescription },
                          { label: "Price", type: "number", value: price, onChange: setPrice },
                        ].map((field, index) => (
                          <Form.Group className='py-2' key={index}>
                            <Form.Label>{field.label}:</Form.Label>
                            <Form.Control
                              type={field.type}
                              placeholder={`Enter ${field.label}`}
                              required
                              value={field.value}
                              onChange={e => field.onChange(e.target.value)}
                            />
                          </Form.Group>
                        ))}
                        <Button variant="danger" type="submit" className="m-3">
                          Confirm
                        </Button>
                      </Form>
                    </Col>
                  </Row>
                </Container>

    </Layout>
                  
                    
                 



  )
}
