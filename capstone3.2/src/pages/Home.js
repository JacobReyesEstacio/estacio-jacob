import React from 'react'
import logoreactproject from '../images/logoreactproject.png'

function Home() {
  return (
    <>
    <div className='logo'>
      <img className='logoImg' src={logoreactproject} alt='LOGO' />
    </div>
    <h1 className='text-center custom-heading'>About us</h1>
    </>
  )
}

export default Home
