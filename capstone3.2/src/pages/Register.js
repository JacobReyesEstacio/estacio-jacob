import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){

	const {user} = useContext(UserContext);


    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");	


    const [isActive, setIsActive] = useState(false);

  
    console.log(email);
    console.log(password);
    console.log(confirmPassword);


    function registerUser(e){
    	
    	e.preventDefault();

    	fetch('https://cpston2-ecommerceapi-estacio.onrender.com/users/register',{
    		method: 'POST',
    		headers:{
    			"Content-Type":"application/json"
    		},
    		body: JSON.stringify({
    			email: email,
    			password: password

    		})
    	})
    	.then(res=>res.json())
    	.then(data=>{

    		console.log(data)
    		if(data){

                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Happy eating!"
                })
    			setEmail('');
    			setPassword('');
    			setConfirmPassword('')
				
    		}else{
    			Swal.fire({
                    title : "Registration failed",
                    icon: "error",
                    text: "Email already in-use Please Try again!"
                })
    		}
    	})
    }

    useEffect(()=>{

    	if((email !==""  && password !=="" && confirmPassword !== "") && (password === confirmPassword)){

    		setIsActive(true)

    	}else{
    		setIsActive(false)
    	}

    },[email,password,confirmPassword])



	return(
		(user.id !== null) ?
			<Navigate to="/login" />
		:
		<Form className='offset-md-4 col-12 col-md-4 custom-heading' onSubmit={(e)=>registerUser(e)}>
			<h1 className="my-5 text-center custom-heading">Register</h1>
			<Form.Group className='my-1'>
				<Form.Label><b className='custom-heading'> Email: </b></Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e=>{setEmail(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group className='my-1'>
				<Form.Label><b className='custom-heading'> Password: </b></Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e=>{setPassword(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group className='my-1'>
				<Form.Label><b className='custom-heading'> Confirm Password: </b></Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm Password"
					required
					value={confirmPassword}
					onChange={e=>{setConfirmPassword(e.target.value)}}
				/>
			</Form.Group>

			{
				isActive ?
				<Button variant="dark" type="submit" id="submitBtn" className='my-3'>Submit</Button>
				:
				<Button variant="secondary" type="submit" id="submitBtn" className='my-3' disabled>Submit</Button>
			}

		</Form>
	)
}