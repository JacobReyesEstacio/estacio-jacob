import { useState, useEffect } from 'react';
import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import AddToCart from '../components/AddToCart';


export default function ProductView() {

	const { productId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	
	  

	useEffect(() => {
		console.log(productId);

		fetch(`https://cpston2-ecommerceapi-estacio.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.productName);
			setDescription(data.productDescription)
			setPrice(data.productPrice);
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						{
								localStorage.getItem("token") !== null ? (
										<AddToCart productId={productId} />
								) : (
									<Link to="/login">
									<Button variant="success">Login</Button>
									</Link>
								)
						}							
					</Card.Body>
				</Col>
			</Row>
		</Container>
	)
}
