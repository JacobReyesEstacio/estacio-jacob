import Banner from '../components/Banner';

export default function Home() {


const data = {
    title: "Ai.Co",
    content: "Freshly baked for your cravings",
    destination: "/#",
    label: "Order now!"
}

	return (
		<>
			<Banner data={data} />
		</>
		
	)
}