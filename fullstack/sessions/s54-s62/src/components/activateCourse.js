import React from "react";
import Swal from "sweetalert2";

export default function ArchiveCourses({ courseId, isActive, fetchData }) {
  const archiveToggle = () => {
    fetch(`http://localhost:4000/courses/${courseId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Course successfully archived!",
          }).then(() => {
            fetchData();
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Failed to archive course. Please try again.",
          });
        }
      });
  };

  const activateToggle = () => {
    fetch(`http://localhost:4000/courses/${courseId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Course successfully activated!",
          }).then(() => {
            fetchData();
          });
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "Failed to activate course. Please try again.",
          });
        }
      });
  };

  return (
    <div>
      {isActive ? (
        <button className="btn btn-danger" onClick={archiveToggle}>
          Archive
        </button>
      ) : (
        <button className="btn btn-primary" onClick={activateToggle}>
          Activate
        </button>
      )}
    </div>
  );
}