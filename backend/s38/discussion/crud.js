let http = require('http');
let port = 4000;

let directory = [
	{
		"name":"Zara Evergreen",
		"email":"zara.evergree@gmail.com"
	},

	{
		"name":"Joy Boy",
		"email":"joyboy@gear5.com"
	}

]

let app = http.createServer((request, response)=>{

	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200,{'Content-type':'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	else if (request.url == "/users" && request.method == "POST"){

		let requestBody = '';


		request.on('data',(data)=>{
			console.log(`This is the data received from the client ${data}`)
			console.log(data);
			requestBody += data;
		})

		request.on('end',()=>{
			requestBody = JSON.parse(requestBody);

			let newUser = {
			"name":requestBody.name,
			"email":requestBody.email
				}

			directory.push(newUser);	

			response.writeHead(200,{'Content-type':'application/json'});
			response.write(JSON.stringify(directory));
			response.end();
		})

		


	}

})

app.listen(port,()=>console.log(`Server running at localhost: ${port}`));

