//console.log("Hello World 21");


/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

function getUserInfo() {
    let user = {
        
        name: "jake",
        age: 31,
        address: "229 Kanlaon Street Quezon City",
        isMarried: true,
        petName: "Chiko"
    };
    return user;
}

let userInfo = getUserInfo();
console.log("getUserInfo();");
console.log(userInfo);


/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

function getArtistsArray() {
    let artists = ["GDragon", "IU", "Bamboo", "BlackPink", "Eminem"];
    return artists;
}

let favoriteArtists = getArtistsArray();
console.log("getArtistsArray();");
console.log(favoriteArtists);

/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

function getSongsArray() {
    let songs = ["Crooked", "Palette", "Hallelujah", "Whistle", "Lose Yourself"];
    return songs;
}

let favoriteSongs = getSongsArray();
console.log("getSongsArray();");
console.log(favoriteSongs);


/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/
 function getMoviesArray() {
    let movies = ["Titanic","Greatest Showman","Avengers Endgame","One Piece Film Red","One more chance"];
    return movies;
 }

 let favoriteMovies = getMoviesArray();
 console.log("getMoviesArray();");
 console.log(favoriteMovies);


/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/

function getPrimeNumberArray() {
    let primes = [2,3,5,7,17];
    return primes;
}
primeNumbers = getPrimeNumberArray();
console.log("getPrimeNumberArray();");
console.log(primeNumbers);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
        getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
        getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
        getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
        getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

    }
} catch(err){


}