function greeting() {
    console.log("Impossible is nothing");
}


let countNum = 5;
while (countNum !== 0){
    greeting();
    countNum--;
}

let count = 5;

/* 
while (expression / condition) {
    statement
}
*/



while (count !== 0) {
    console.log("While: "+ count);
    count--;
}


/* 
do {
    statement
}while(expression/condition)

*/

/* let number = Number(prompt("Choose a number: "));


do {
    console.log("Do while: "+ number);
    number+=1;
}while (number <10); */



/* 
for (initialization; condition; finalexpression){
    statement
}
*/


for (let i=0; i<=20; i++) {
    console.log("For: "+ i);
}


//strings

let myString = "Taylor Swift";

//characters in strings may be counted using the .length property.
console.log(myString.length);

//accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);


for(let x = 0; x < myString.length; x++){
    console.log(myString[x]);
}

let myName = "Jacob Estacio";

for (let i = 0; i<myName.length; i++) {

  if  ( myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u")
    {
        console.log(3);
    }
    else {
        console.log(myName[i]);
    }

}



for (let count = 0; count <= 20; count++){
    if (count % 2 === 0){
        continue;
    }
    console.log("Continue and Break: " + count);

    if (count>10){
        break;
    }
}


let fname = "Bernardo";

for (i = 0; i < fname.length; i++) {
    if (fname[i].toLowerCase() == "a" ) {
       console.log("continue to the next iteration");
        continue;
        
    }
    console.log(fname[i]);
   
        if (fname[i].toLowerCase() == "d"){
          
            break;
        } 
}

