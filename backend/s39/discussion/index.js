console.log(fetch('https://jsonplaceholder.typicode.com/posts'))


fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((posts)=>console.log(posts))

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);
	console.log(typeof result);
	console.log(result.body);
	let gdragon = await result.json()
	console.log(gdragon);
}

fetchData();

//post

fetch('https://jsonplaceholder.typicode.com/posts',{
	method: 'POST',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello world!',
		userId: 1
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


//update

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));

//delete

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
})

//filtering

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=6')
.then((response)=>response.json())
.then((json)=>console.log(json))

//nested comments

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json))