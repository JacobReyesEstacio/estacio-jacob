//Syntax Statements and Comments

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

// Statements in programming are instructions that we tell the computer to perform
// JS Statements usually end with semicolon (;)
// Syntax - it is a set of rules that describes how statements must be constructed.
// Comments ctrl + /

/*console.log("Hello!");

alert("this is an alert!");
alert("this is another alert!");*/

let myVariable;
console.log(myVariable);

// declaring and initializing variable
// let / const 

let productName = 'Desktop Computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);


// const values that cannot change
const interest = 3.539;


// re-assigning values

productName = 'laptop';
console.log(productName);

let friend = 'Kate';
friend = 'jane';

const pi = 3.1416;
console.log(pi);

let productCode = 'DC017'; const productBrand = 'Dell';
console.log(productCode, productBrand);

// Data Types

// Strings

// \n = new line

let country = 'Philippines';
let province = "Metro Manila";
let fullAddress = province + ',' + country; 
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

let mailAddress = province + '\n\n' + country;
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home late';
console.log(message);


// numbers

let headcount = 26;
let grade = 98.7;
console.log(headcount);
console.log(grade);
let planetDistance = 2e10;
console.log(planetDistance);


console.log("John's firt grade last quarter is "+ grade);

// Boolean

let isMarried = false;
let isGoodConduct = true;
console.log(isMarried);
console.log(isGoodConduct);
console.log('is good conduct: '+ isGoodConduct);


// arrays
let grades = [98,97,77,89,98.6];

// Objects

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.7,
    fourthGrading: 94.6

}




let person = {

    fullName: 'Juan Dela Cruz',
    age: 35,
    isMarried: false,
    contact: ["09988987656", "87000"],
    address: {
        houseNumber: '345',
        city: 'Manila'
    }

}

console.log(person);
console.log(myGrades);

console.log(typeof person);