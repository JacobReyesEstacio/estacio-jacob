//console.log("s22")

/* function printName() {
    let nickname = prompt("Tenter your nickname: ");
    console.log("hi, " + nickname);
}

printName(); */


function printName(name) {
    
    console.log("hi, " + name);
}

printName("jake"); 

function checkDivisibilityBy8 (num){
   let remainder = num % 8;
   let isDivisibleBy8 = remainder === 0;
   console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(989);
checkDivisibilityBy8(987786677);


console.log("mini activity: \n")

function checkDivisibilityBy4 (num1){
    let remainder = num1 % 4;
    let isDivisibleBy4 = remainder === 0;
    console.log(isDivisibleBy4);
 }

 checkDivisibilityBy4(56);
 checkDivisibilityBy4(95);
 checkDivisibilityBy4(444);

 function argumentFunction() {
    console.log("Thi function was passed as an argument before the message was printed.")
 };

 function invokeFunction(argumentFunction){
    argumentFunction();
 }

 invokeFunction(argumentFunction);


 function createFullName(firstName,middleName,lastName) {
    console.log(firstName+' '+middleName+' '+lastName);
 };

 createFullName('Juan','dela','cruz');
 createFullName('cruz','dela','juan');
 createFullName('juan','dela');
 createFullName('juan','dela','cruz','III');
 createFullName('juan',' ','cruz');

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function printFriends(friend1, friend2, friend3){
    console.log("my 3 Friends are: "+friend1+","+friend2+","+friend3)
   
}

printFriends("Rap","Dust","Cy");

function returnFullName(firstName, middleName, lastName){
    return firstName + ' ' + middleName + ' ' + lastName
    console.log("this will not be printed")
}
let completeName1 = returnFullName("Monkey","D","Luffy");
let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

console.log(completeName1+" is the pirate king");
console.log(completeName2+" is immortal");

console.log("\nmini activity\n\n");

function areaSquare(side){
    return side * side;
}
square = areaSquare(4);
console.log("The area of square with length of 4 is: "+square);

function addNumbers (num1,num2,num3) {
    return num1 + num2 + num3;
}

let added = addNumbers(1,2,3);
console.log("3 numbers added total to: "+added);

function checkHundred (num) {
    return num === 100;
}
let isEqual100 = checkHundred(200);
console.log("is this 100? "+ isEqual100);
