console.log("DOM Manipulation");



console.log(document);
console.log(document.querySelector('#clicker'));

let counter = 0;

const clicker = document.querySelector('#clicker');

	clicker.addEventListener('click',()=>{
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times`);
	})

	const txtFirstName = document.querySelector("#txt-first-name");
	const txtLastName = document.querySelector("#txt-last-name");
	const spanFullName = document.querySelector("#span-full-name");


function updateFullName() {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

const labelFirstName = document.querySelector('#label-text-first-name');

labelFirstName.addEventListener('mouseover',(e)=>{
	//alert("You hovered the first name label");
})