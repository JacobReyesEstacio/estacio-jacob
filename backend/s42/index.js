const express = require("express")
const mongoose = require("mongoose")

const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 4000;


app.use(express.json());
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://Jacob:flr88ax45bvg@cluster0.q6y4s9e.mongodb.net/taskDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

// If a connnection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"))

app.use("/tasks", taskRoute);









if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;
