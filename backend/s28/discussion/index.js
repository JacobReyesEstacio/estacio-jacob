console.log("Array non-mutator and iterator methods");

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: '+ firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);


countries.forEach(function(country){
	console.log(country);
});

// map method like forEach but requires use of return

let numbers = [ 1,2,3,4,5 ];

let numberMap = numbers.map(function(number){

	return number**2;
})

console.log("Original Array: ")
console.log(numbers);
console.log("Result of map method: ")
console.log(numberMap);

let numbersforEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numbersforEach)

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("result of every method");
console.log(allValid);

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log("result of some method");
console.log(someValid);



let filterValid = numbers.filter(function(number){
	return (number < 3);
})

console.log("result of filter method");
console.log(filterValid);

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound = products.includes("Mouse");
console.log(productFound);

let productFound2 = products.includes("Headset");
console.log(productFound2);

let filteredProducts = products.filter(function(product){

		return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

let list = ['Hello', 'from', 'the', 'other', 'side'];

let reducedJoin = list.reduce(function(x,y){
	console.log(x);
	console.log(y);
	return x + ' ' + y;
})

console.log("Reduce method: "+ reducedJoin);