/* console.log("Hello B297");

let numG = -1;
let numH = 1;

if (numG>0) {
    console.log("Hello the condition in the if statement is true");
}

if (numG > 0) {
    console.log("Hello!");
}

else if (numH = 0) {
    console.log("else if is true");
}

else {
    console.log ("hello from the other side");
}

let message = 'no message';
/*  */

function determineTyphoonIntensity (windspeed) {
    if (windspeed<30){
        return 'Not a typhoon yet';
    }
    else if (windspeed <= 61) {
        return 'Tropical Depression detected!';
    }
    else if (windspeed >=62 && windspeed <= 88){
        return 'Tropical storm detected';
    }
    else {
        return 'Typhoon detected';
    }
}

message = determineTyphoonIntensity(65);
console.log(message);

//console.warn() is a good way to print warnings in our console that could help us devs act on certain output within our code 

if (message == 'Tropical Storm detected!'){
    console.warn(message);
}

// [truthy and falsy]
/*
        in JS a "truthy" value is a value that is considered true when encountered in a Boolean ontext 
        value are considered true unless defined false 

        falsy values/exception for truthy:
        1. false
        2. 0 
        3 -0
        4. ""
        5. null
        6. undefined 
        7. nan
*/

//Truthy Examples 
/* if (true){
    console.log('This is truthy!')

}

if (1){
    console.log('this is truthy!')
}

if ([]){
    console.log('this is truthy!')
}

if ("jake") {
    console.log("truty")
}


let ternaryResult = (1 < 18) ? true : false;
console.log ("Result of ternary operator: " + ternaryResult);





let name;

function isOfLegalAge () {
    name = 'John';
    return 'You are of the legal age limit!';
}

function isUnderAge() {
    name = 'Jane';
    return "You are under the age limit";
} */

/* let age = parseInt (prompt("What is your age: "));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log ("Result of Ternary operator in function: " + legalAge + ',' + name); 

/*

    switch (expression) {
        
        case value:
            statement;
            break

        default:
            statement;
            break;

    }


*/
/* 
let day = prompt ("What day of the week is it today?").toLowerCase();
console.log(day);


    switch (day) {
        case 'monday' :
            console.log("The color of the day is blue!");
            break;   
        case 'tuesday' :
            console.log('The color of the day is yellow');
            break;
        case 'wednesday' :
            console.log('The color of the day is red');
            break;
        case 'thursday' :
            console.log('The color of the day is pink');
            break;
        case 'friday' :
            console.log('The color of the day is purple');
            break;    
         case 'saturday' :
            console.log('The color of the day is green');
            break;
        case 'sunday' :
            console.log('The color of the day is white');
            break;  
        default :
            console.log("Please input a valid day");
    }
 */
    function showIntensityAlert(windspeed) {
        try {
            alerat(determineTyphoonIntensity(windspeed));
        }
        catch(error){
            console.log(typeof error);
            //"error.message is used to access the information relating to the error obkect"
            console.log(error.message);
        }
        finally {
            alert('intensity updates will show new alert');
        }

        showIntensityAlert(56);
    }