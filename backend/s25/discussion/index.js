console.log("javascript objects")


/*
syntax 
let objectName = {
	keyA: valueA,
	keyB: valueB
}

*/

let ninja = {
	name: "Naruto",
	village: "Konoha",
	occupation: "Hokage"
}

console.log("Result from creating objects using initializers/literal notation");
console.log(ninja);
console.log(typeof ninja);

let dog = {
	name: "whitey",
	color: "white",
	breed: "chihuahua"
}

/*
create reusable function to create several objects that have the same data structure.

*/




function Laptop(name,manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop1 = new Laptop('lenovo', 2023);
console.log("Result from creating objects using object constructor");
console.log(laptop1);


let myLaptop = new Laptop('Macbook air', 2020);
console.log("Result from creating objects using object constructor");
console.log(myLaptop);

/*let oldLaptop = new Laptop ('Portal R2E CCMC', 1980);
console.log("Result from creating objects using object constructor");
console.log(oldLaptop);*/


let laptop2 = new Laptop('Dell',2019);
let laptop3 = new Laptop('Asus',2012);
let laptop4 = new Laptop('Acer',2016);
console.log(laptop2);
console.log(laptop3);
console.log(laptop4);


let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);


//access object information
console.log ("Result: "+ myLaptop.name);  //data notation
console.log("Result: "+ laptop1['name']);

console.log ("Result: "+ myLaptop.manufactureDate);
console.log("Result: "+ laptop1['manufactureDate']);



let array = [laptop1, myLaptop];


console.log(array[0]['name']);
console.log(array[0].name);

let car = {};

car.name = 'Honda Civic';
console.log("result from adding properties using dot notation: ");
console.log(car);



car['manufacture date'] = 2019;
console.log(car['manufacture date']);
delete car['manufacture date'];
console.log(car);
car.name = "dodge charger";
console.log(car);


/*
	A method is a function which is a property of an object similar to functions/features of real world objects,
	methods are defined based on what an object is capable of doing ad how it should work.
 

*/

let person = {
	name: "cardo",
	talk: function() {
		console.log('Hello my name is '+ this.name );
	}
}

console.log(person);
console.log("result of object methods");

person.talk();

person.walk = function() {
	console.log(this.name + ' walked 25 steps forward');
}

person.walk();

let friend = {
	firstName: 'Nami',
	lastName: 'Misko',
	address: {
		city: 'Tokyo',
		country: 'Japan'
	},
	emails: ['nami.@sea.com','namimisko@gmail.com'],
	introduce: function() {
		console.log('Hello! my name is '+ this.firstName + ' ' + this.lastName);
	}
}

friend.introduce();



//objct literals
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This Pokemon tackled Taget Pokemon!")
		console.log("Target Pokemon's health is now reduced to Target Pokemon health")
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);


//create an object constructor

function Pokemon(name,level) {
	//properties
	this.name= name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
	}
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}

let pikachu = new Pokemon("Pikachu",16);
let rattata = new Pokemon("Rattata", 8);
pikachu.tackle(rattata);
rattata.faint();