//Servers respond differently with different requests

const http = require('http');
const port = 4000;

const app = http.createServer((request,response)=>{

	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('Hello, B297')
	}
	else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('This is the homepage')
	}
	else{
		response.writeHead(404,{'Content-type':'text/plain'})
		response.end('Page not available, try other routes')
	}

})

app.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);