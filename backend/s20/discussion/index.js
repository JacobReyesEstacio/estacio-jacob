console.log('hello world');

// Arithmetic Operators
// +, -, *, /, &

let x = 1397;
let y = 7831;

let sum = x + y;
let difference = x - y;
let product = x * y;
let quotient = x / y;
let remainder = x % y;



console.log('Result of addition operator: ', sum);
console.log('Result of subtraction operator: ', difference);
console.log('Result of multiplication operator: ', product);
console.log('Result of division operator: ', quotient);
console.log('Result of modulo operator: ', remainder);

let a = 10; 
let b = 5;

let remainderA = a % b;



//Assignment Operator
// = basic assignment operator

// addition assignment operator

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;

console.log(assignmentNumber);

// subtraction assignment operator

assignmentNumber-=2;

console.log("The result of -= : ", assignmentNumber)


// multiplication assignment operator


assignmentNumber*=2;

console.log("The result of *= : ", assignmentNumber)

// division assignment operator

assignmentNumber/=2;

console.log("The result of /= : ", assignmentNumber)


//multiple operators

let mdas = 1 + 2 - 3 * 4 / 5;

console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);

console.log(pemdas);


//increment and decrement
// operators that add or subtract values by 1 and re-assigns the value of the variable where the increment / decrement was applied to.

let z = 1;

let increment = z++;

console.log("Result of pre-increment " + increment);
console.log(z);

increment = ++z;
console.log("Result of post-increment " + increment);
console.log(z);


let decrement = --z;

console.log("Result of pre-decrement " + decrement);
console.log(z);

decrement = z--;
console.log("Result of post-decrement " + decrement);
console.log(z);


// type coercion

// coercion is the automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;

console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// boolean values true = 1 / false = 0

//Comparison operators

let juan = 'juan';

//equality operators

console.log (1 == 1);
console.log (1 == 2);
console.log (1 == '1');
console.log (0 == false);
console.log ('juan' == 'juan');
console.log ('juan' == juan);

//inequality operators

console.log (1 != 1);
console.log (1 != 2);
console.log (1 != '1');
console.log (0 != false);
console.log ('juan' != 'juan');
console.log ('juan' != juan);




//strict equality operators

console.log (1 === 1);
console.log (1 === 2);
console.log (1 === '1');
console.log (0 === false);
console.log ('juan' ==='juan');
console.log ('juan' === juan);

//strict inequality operators

console.log (1 !== 1);
console.log (1 !== 2);
console.log (1 !== '1');
console.log (0 !== false);
console.log ('juan' !== 'juan');
console.log ('juan' !== juan);


//relational operators
console.log("relational operators");
let abc = 50;
let def = 65;

let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";

console.log(abc > numStr); //forced coercion
console.log(def <= numStr);





