import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';


export default function AdminView({ productsData, fetchData }) {

    // b. Add state to store all courses 
    const [products, setProducts] = useState([])


    //Getting the coursesData from the courses page
    useEffect(() => {
        const productsArr = productsData.map(product => {
            return (
                <tr key={products._id}>
                    <td>{products._id}</td>
                    <td>{products.productName}</td>
                    <td>{products.productDescription}</td>
                    <td>{products.productPrice}</td>
                    <td className={products.isActive ? "text-success" : "text-danger"}>
                        {products.isActive ? "Available" : "Unavailable"}
                    </td>
{/*                     <td><EditCourse course={course._id} fetchData={fetchData}/></td> 
                    <td><ArchiveCourse course={course._id} isActive={course.isActive} fetchData={fetchData}/></td>    */}
                </tr>
                )
        })

        setProducts(productsArr)

    }, [productsData])


    return(
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>
            
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {products}
                </tbody>
            </Table>    
        </>

        )
}
