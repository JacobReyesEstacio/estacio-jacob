import Banner from "../components/Banner"


export default function Home() {


    const data = {
        title: "Ai.Co",
        content: "Baking Dreams Come True",
        destination: "/",
        label: "Order now!"
    }
    
        return (
            <>
                <Banner data={data} />
             
            </>
            
        )
    }